<?php

namespace Modules\UserTask\Console;

use Illuminate\Console\Command;
use Nwidart\Modules\Facades\Module;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class InstallUserTask extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'install:usertask';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installe le module de gestion de tache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(setting('module.user.task.active') == 0) {
            $module = Module::find('UserTask');
            $module->enable();
            $this->call("migrate");
            setting(["module.user.task.active" => 1])->save();
            $this->info("Le module 'User Task' est activé");
        }else{
            $this->error("Le module est déja installer");
        }
    }
}
